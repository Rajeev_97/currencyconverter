public with sharing class CurrencyController {

    @AuraEnabled
    public static List<Contact> getCurrency() {
        return [
                SELECT
                        Price__c,
                        convertCurrency(Price__c) ConvertedPrice__c
                FROM Contact
                WHERE LastName = 'TestC'
        ];
    }
}