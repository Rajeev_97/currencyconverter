import { LightningElement,wire,track} from 'lwc';
import getCurrency from '@salesforce/apex/CurrencyController.getCurrency';
export default class CurrencyController extends LightningElement {
    @track columns ;
    @track error ;
    @track data ;
    get columns[
        { label: 'Price', fieldName: 'Price__c', type: 'text' },
        { label: 'ConvertedPrice', fieldName: 'ConvertedPrice__c', type: 'text' },
    ]
    @wire(getCurrency)
    wiredContacts({
        error,
        data
    }) {
        if (data) {
            this.data = data;
            //console.log(JSON.stringify(data, null, '\t'));
        } else if (error) {
            this.error = error;
        }
    }
}